package com.company;

class Player {

    //Fields
    private int score;
    private String name;

    //Constructor
    Player(String name){
        this.name = name;
    }

    //Getter
    String getName(){
        return name;
    }
    int getScore(){
        return score;
    }

    //Setter
    void setName(String name){
        this.name = name;
    }
    void setScore(int score){
        this.score = score;
    }

}
