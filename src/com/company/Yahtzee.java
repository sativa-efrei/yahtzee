package com.company;


import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;


class Yahtzee{

    //Fields
    private Object[][] data = {
         {"Ones", " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"Twos", " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"Threes", " "," ", Boolean.FALSE, Boolean.FALSE,},
         {"Fours", " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"Fives", " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"Sixes", " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"Three of a kind", " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"Four of a kind",  " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"Full House",  " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"Small straight",  " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"Large straight",  " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"Chance",  " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"YAHTZEE",  " ", " ", Boolean.FALSE, Boolean.FALSE,},
         {"SUM", " ", "", " ", ""}
    };
    private String[] columnNames = {"Figures",
            "Player 1", "Player 2", "Check 1", "Check 2"};
    private Player[] player;
    private Dice listDices;
    private JButton[] rollButton = new JButton[2];
    private JTable scoreTable;
    private ArrayList<ArrayList<Integer>> listOccurrence = new ArrayList<>();
    private int numberOfFiguresChecks;
    private int[] scoreTotal;
    private int turn;

    //Constructor
    Yahtzee(String name, String name2){
        player = new Player[2];
        player[0] = new Player(name);
        player[1] = new Player(name2);
        listDices = new Dice();
        rollButton[0] = new JButton("Roll Dice");
        rollButton[1] = new JButton("Roll Dice");
        numberOfFiguresChecks = 0;
        scoreTotal = new int[2];
        turn = 0;

        //Change name value
        columnNames[1] = player[0].getName();
        columnNames[2] = player[1].getName();
        //init score value

        scoreTable = new JTable(data, columnNames){
            @Override
            public boolean isCellEditable(int row, int column) { //override => cell isn't editable
                return false;
            }
        };
        scoreTable.removeColumn(scoreTable.getColumnModel().getColumn(3));
        scoreTable.removeColumn(scoreTable.getColumnModel().getColumn(3));

        scoreTable.getColumnModel().getColumn(1).setCellRenderer(new CustomRenderer());
        scoreTable.getColumnModel().getColumn(2).setCellRenderer(new CustomRenderer());

    }
    Yahtzee(String name){
        player = new Player[1];
        player[0] = new Player(name);
        listDices = new Dice();
        rollButton[0] = new JButton("Roll Dice");
        numberOfFiguresChecks = 0;
        scoreTotal = new int[1];
        turn = 0;
        columnNames[1] = player[0].getName();
        scoreTable = new JTable(data, columnNames){
            @Override
            public boolean isCellEditable(int row, int column) { //override => cell isn't editable
                return false;
            }
        };
        scoreTable.removeColumn(scoreTable.getColumnModel().getColumn(3));
        scoreTable.removeColumn(scoreTable.getColumnModel().getColumn(3));

        scoreTable.getColumnModel().getColumn(1).setCellRenderer(new CustomRenderer());

    }

    //Getter
    Object[][] getData(){
        return data;
    }
    Object getData(int row, int col){
        return data[row][col];
    }
    Player getPlayer(int idx){ return player[idx]; }
    Dice getListDices(){ return listDices; }
    JTable getTable(){ return scoreTable; }
    JButton getRollButton(int idx){ return rollButton[idx]; }
    ArrayList<ArrayList<Integer>> getListOccurrence(){ return listOccurrence; }

    int getNumberOfFiguresChecks(){ return numberOfFiguresChecks; }
    int[] getScoreTotal(){ return scoreTotal; }
    int getScoreTotal(int idx){ return scoreTotal[idx]; }
    int getTurn(){ return turn; }

    //Setter
    void setData(int row, int col, Object value){
        data[row][col] = value;
    }
    void setScoreTotalInc(int idx, int value){ scoreTotal[idx] += value; }
    void setScoreTotal(int idx, int value){ scoreTotal[idx] = value; }
    void setNumberOfFiguresChecks(){ numberOfFiguresChecks++; }
    void setTurn(int value){ turn = value; }
    void setTurn(){ turn++; }

    //Methods
    void occurrence() {
        ArrayList<Integer> one = new ArrayList<>();
        ArrayList<Integer> two = new ArrayList<>();
        ArrayList<Integer> three = new ArrayList<>();
        ArrayList<Integer> four = new ArrayList<>();
        ArrayList<Integer> five = new ArrayList<>();
        ArrayList<Integer> six = new ArrayList<>();
            for(int i = 0; i < listDices.getDices().length; i++){
                switch (listDices.getDicesIdx(i)){
                    case 1:
                        one.add(listDices.getDicesIdx(i));
                        break;
                    case 2:
                        two.add(listDices.getDicesIdx(i));
                        break;
                    case 3:
                        three.add(listDices.getDicesIdx(i));
                        break;
                    case 4:
                        four.add(listDices.getDicesIdx(i));
                        break;
                    case 5:
                        five.add(listDices.getDicesIdx(i));
                        break;
                    case 6:
                        six.add(listDices.getDicesIdx(i));
                        break;
                }
            }
            listOccurrence.add(one);
            listOccurrence.add(two);
            listOccurrence.add(three);
            listOccurrence.add(four);
            listOccurrence.add(five);
            listOccurrence.add(six);
    }
    void remove_occurence_list(){
        for(int i = 0; i < listOccurrence.size(); i++){
            for(int j = 0; j < listOccurrence.get(i).size(); j++){
                listOccurrence.get(i).clear();
            }

        }
        listOccurrence.clear();
        System.out.println("");
    }
    void clean_table(){
        for(int i = 0; i < scoreTable.getRowCount(); i++){
            if(data[i][3].toString().equals("false"))
                scoreTable.setValueAt(" ", i, 1);
            if(data[i][4].toString().equals("false") && Main.multiplayer)
                scoreTable.setValueAt(" ", i, 2);
        }
    }
    void print_data(){
        for(int i = 0; i < data.length; i++){
            System.out.print("{ ");
            for(int j = 0; j < data[i].length; j++){
                System.out.print(data[i][j]+" , ");
            }
            System.out.print(" }\n");
        }
    }

    //Other class
    class CustomRenderer extends DefaultTableCellRenderer {

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
            Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            setHorizontalAlignment(JLabel.CENTER);
            //System.out.println(value);
            if(row != scoreTable.getRowCount() - 1) {
                if (!scoreTable.getValueAt(row, column).toString().equals(" ") && data[row][column + 2].toString().equals("true")) {
                    cellComponent.setForeground(new Color(0, 230, 118));
                } else
                    cellComponent.setForeground(Color.red);
            }
            else  cellComponent.setForeground(Color.black);
            return cellComponent;
        }
    }

}
