package com.company;

import java.util.Scanner;
import javax.swing.*;

class Dice extends JFrame {

    //Fields
    private int[] dices;
    private int idx;
    private boolean[] locked = new boolean[5];
    private ImageIcon[] imagesButtons = new ImageIcon[5];
    private JButton[] dicesButtons = new JButton[5];

    //Constructor
    Dice(){
        dices = new int[5];
        for(int i = 0; i < dices.length; i++){
            dices[i] = 1 + (int)(Math.random() * ((6 - 1) + 1));
            imagesButtons[i] = new ImageIcon("assets\\"+dices[i]+".png");
            dicesButtons[i] = new JButton(imagesButtons[i]);
            dicesButtons[i].setFocusPainted(false);
            dicesButtons[i].setBorderPainted(true);
            dicesButtons[i].setContentAreaFilled(false);
            dicesButtons[i].setActionCommand("Dice= "+i);
            locked[i] = false; //dice enable to be roll
            idx = i;
        }
    }

    //Getter
    int[] getDices(){
        return dices;
    }
    int getDicesIdx(int idx){
        if(idx >= 0 && idx <= 6){
            return dices[idx];
        }
        else return -1;
    }
    boolean getLockedIdx(int idx){
        return locked[idx];
    }
    boolean[] getLocked(){
        return locked;
    }
    ImageIcon[] getImageIcon(){ return imagesButtons; }
    JButton[] getButton(){ return dicesButtons; }
    ImageIcon getImageIconIdx(int idx){ return imagesButtons[idx]; }
    JButton getButtonIdx(int idx){ return dicesButtons[idx]; }

    //Setter
    void setDices(int index, int value){
        dices[index] = value;
    }
    void setLockedIdx(int idx, boolean value){ locked[idx] = value; }

    //Methods
    void reRoll(){
        for(int i = 0; i < dices.length; i++) {
            //if locked[i] is false => new number
            if(!locked[i]) dices[i] = randomNumberDice();
        }
    }


    private int randomNumberDice(){
        return 1 + (int)(Math.random() * ((6 - 1) + 1));
    }
}
