package com.company;



import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import org.testng.annotations.Test;

import java.io.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    /*INIT GLOBAL VARIABLE*/
    private static int spaceBoard = 60;
    private static JFrame windowGame = new JFrame();
    private static Yahtzee game;
    private static Color boardColor = new Color(0, 230, 118);
    private static int player_index = 0;
    static boolean multiplayer = false;

    private static int loop = 0;
    public static void main(String[] args){

        init_frame_game();
    }
    /**
     * LOAD FRAME FUNCTIONS
     */


    /*Initialisation of the frame*/
    private static void init_frame_game(){
        /*INIT FRAME*/
        windowGame.setTitle("Yahtzee Game");
        windowGame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        windowGame.setVisible(true);
        windowGame.setResizable(false);
        windowGame.setSize(1000, 800);

        load_frame_menu();

    }
    /*Loading menu*/
    private static void load_frame_menu(){
        /*INIT PANEL*/
        windowGame.getContentPane().setBackground(boardColor);
        windowGame.setLayout(null);

        //TITLE
        // create a label to display text
        JLabel title = new JLabel();
        // add text to label
        title.setText("Yahtzee");
        title.setFont (title.getFont ().deriveFont (64.0f));
        title.setBounds(windowGame.getWidth()/2-windowGame.getWidth()/8, spaceBoard, windowGame.getWidth()/4,spaceBoard);

        // add label to panel
        windowGame.add(title);

        //BUTTONS MENU
        JButton[] buttons_menu = new JButton[3];
        String[] name = {"Play", "Help", "Quit"};
        for(int i = 0; i < 3; i++){
            buttons_menu[i] = new JButton(name[i]);
            buttons_menu[i].setFont (title.getFont ().deriveFont (32.0f));
            buttons_menu[i].addActionListener(new Action());
            buttons_menu[i].setBounds(windowGame.getWidth()/2-windowGame.getWidth()/8, 250+i*100, windowGame.getWidth()/4,spaceBoard);
            buttons_menu[i].setActionCommand(name[i]);
            windowGame.add(buttons_menu[i]);
        }
        windowGame.repaint();
    }
    /**
     * GAME FUNCTIONS
     */
    /*Choice multi*/
    private static void multiplayer_panel(){
        JLabel title = new JLabel();
        clean_frame(); //remove frame
        //init_frame_game(); //load menu
        String[] name = {"Multiplayer", "Soloplayer"};
        JButton[] buttons = new JButton[2];
        for(int i =0; i < 2; i++){
            buttons[i] = new JButton(name[i]);
            buttons[i].setFont (title.getFont ().deriveFont (32.0f));
            add_button(windowGame.getWidth()/2-windowGame.getWidth()/8, 250+i*100, windowGame.getWidth()/4,spaceBoard, buttons[i]);
        }
        paint_return_button();

    }
    /*Run Yahtzee game*/
    private static void run_game(){

        clean_frame();
        player_index = 0;
        boolean gameAvaible = false;
        String player_one;
        String player_two;
        //get user name
        if(multiplayer){
            player_one = input_user();
            if(player_one != null) {
                player_two = input_user();
                if (player_two != null) {
                    game = new Yahtzee(player_one, player_two); //create yahtzee class
                    gameAvaible = true;
                }
            }
        }
        else{
            player_one = input_user();
            if(player_one != null) {
                game = new Yahtzee(player_one);
                gameAvaible = true;
            }
        }
        //create yahtzee class
        if(gameAvaible) {
            for (int i = 0; i < game.getListDices().getImageIcon().length; i++) {
                game.getListDices().getButtonIdx(i).addActionListener(new Action());
            }
            paint_board();
        }
    }
    /*Update game*/
    private static void update_game(){

        if(game.getTurn() > 2){ //if user plays 3 turns
            game.getRollButton(player_index).setEnabled(false);
        }

        enable_dice(true); //dice enable to be move
        game.clean_table(); //clean table value
        calc_score(); //calculation score
        game.remove_occurence_list(); //remove occurrence array

        //Add a event listener when user click on a cell
        game.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {

                int row = game.getTable().rowAtPoint(evt.getPoint()); //get the row idx
                int col = game.getTable().columnAtPoint(evt.getPoint());//get the column idx
                boolean stepNext = false; //know if the cell is correct when user clicks on it.
                //check column is false
                if (row <= game.getTable().getRowCount() - 1
                        && col == player_index + 1
                        && game.getTable().getModel().getValueAt(row, player_index + 3).toString().equals("false")
                        && !game.getTable().getModel().getValueAt(row, player_index + 1).toString().equals(" ")) {

                        game.setData(row, col, game.getTable().getValueAt(row, col)); //change data value with the score

                        game.getTable().getModel().setValueAt(true, row, player_index + 3); //change the check value to true
                        game.clean_table(); //clean the JTable

                        game.setTurn(0); //turn 0*/
                        calc_sum_score(row, col);
                        //Change player idx
                        if (multiplayer) {
                            if (player_index == 0) {
                                game.getRollButton(player_index).setEnabled(false);
                                player_index = 1;
                            } else {
                                game.getRollButton(player_index).setEnabled(false);
                                player_index = 0;
                            }
                        } else player_index = 0;


                        game.getRollButton(player_index).setEnabled(true); //enable button
                        init_paint_dices(); //replace dices
                        game.setNumberOfFiguresChecks();
                        //if all figures are checked => end game
                        if (game.getNumberOfFiguresChecks() == game.getTable().getRowCount() * 2 - 2 && multiplayer) {
                            reset_game();
                        } else if (game.getNumberOfFiguresChecks() == game.getTable().getRowCount() - 1 && !multiplayer)
                            reset_game();
                }
            }
        });
    }
    /*Reset game*/
    private static void reset_game(){
        int input;
        game.getRollButton(0).setEnabled(false);  //disable button roll

        //If the game is multiplayer
        if(multiplayer) {
            game.getRollButton(1).setEnabled(false); //disable button roll
            if (game.getScoreTotal(0) > game.getScoreTotal(1)) //if player 1 wins
                input = JOptionPane.showOptionDialog(null, game.getPlayer(0).getName()+" wins ! \n" + game.getScoreTotal(0)+ " points\nRestart a game ?", "Yahtzee Game", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            else if(game.getScoreTotal(0) < game.getScoreTotal(1))//player 2 wins
                input = JOptionPane.showOptionDialog(null, game.getPlayer(1).getName()+" wins ! \n"+ game.getScoreTotal(1) + " points\nRestart a game ?", "Yahtzee Game", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            else //draw
                input = JOptionPane.showOptionDialog(null, "Draw !\n" + game.getScoreTotal(1) + " points\nRestart a game ?", "Yahtzee Game", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

        }
        else //if the game is soloplayer
            input = JOptionPane.showOptionDialog(null, "Good game !\n" + game.getScoreTotal(0) + " points\nRestart a game ?", "Yahtzee Game", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

        if(input == JOptionPane.OK_OPTION) {
            game.setTurn(0);
            if(multiplayer){
                game.setScoreTotal(0, 0);
                game.setScoreTotal(1, 0);
            } else game.setScoreTotal(0, 0);
            run_game();
        }
        else if(input == JOptionPane.CANCEL_OPTION){
            clean_frame();
            init_frame_game();
        }
    }

    /**
     * CALCUL SCORE FUNCTIONS
     */
    /*calculation score*/
    private static void calc_score(){
        //Calculation number of occurrence
        game.occurrence();
        calc_chance();
        calc_same_numbers();
        calc_straight();
        calc_bonus_kind();
        calc_other_cell();

    }
    /*Calculation sum score*/
    private static void calc_sum_score(int row, int col){
        game.setScoreTotalInc(player_index, Integer.parseInt(game.getData(row, col).toString()));
        game.getTable().setValueAt(game.getScoreTotal(player_index), game.getTable().getRowCount()-1, player_index+1);
    }
    private static void calc_bonus(){
        int result = 0;
        for(int i = 0; i < 6; i++){
            if(!game.getTable().getValueAt(i, player_index+1).toString().equals(" ")){
                result += Integer.parseInt(game.getTable().getValueAt(i, player_index+1).toString());
            }
        }
        if(result >= 63){
            game.setScoreTotalInc(player_index, 32);
        }
    }
    /*Calculation empty cell */
    private static void calc_other_cell(){
        boolean choice = false;
        for(int i = 0; i < game.getTable().getRowCount(); i++){
            if(!game.getTable().getValueAt(i, player_index+1).toString().equals(" ") && game.getData(i, player_index+3).toString().equals("false")){
                //game.getTable().setValueAt(0,i,player_index+1);
                choice = true;
            }
        }
        if(!choice){ //If there a no choice
            for(int i = 0; i < game.getTable().getRowCount(); i++){
                if(game.getTable().getValueAt(i, player_index+1).toString().equals(" ") && game.getData(i, player_index+3).toString().equals("false")){
                    game.getTable().setValueAt(0,i,player_index+1);
                }
            }
        }
    }
    /*Calculation chance*/
    private static void calc_chance(){
        int score = 0;
        for(int i = 0; i < game.getListDices().getDices().length; i++)
            score += game.getListDices().getDicesIdx(i);
        if(cell_empty(11))
            game.getTable().setValueAt(score, 11,player_index+1);
    }
    /*Calculation occurrences and figures with same numbers (Yahtzee and Full House included)*/
    private static void calc_same_numbers(){
        int score = 0;
        boolean tripleDice = false;
        boolean doubleDice = false;
        for(int i = 0; i < game.getListOccurrence().size(); i++){
            score = 0;
            //Yahtzee
            if(game.getListOccurrence().get(i).size() == 5){ //not validated yet
                if(cell_empty(12))
                    game.getTable().setValueAt(50,12,player_index+1);
                else{
                    game.setScoreTotalInc(player_index, 100);
                    game.getTable().setValueAt(Integer.parseInt(game.getTable().getValueAt(12, player_index+1).toString())+100,12,player_index+1);
                }
            }
            //Full House
            if(game.getListOccurrence().get(i).size() == 3 && cell_empty(8)) tripleDice = true;
            if(game.getListOccurrence().get(i).size() == 2 && cell_empty(8)) doubleDice = true;
            for(int j = 0; j < game.getListOccurrence().get(i).size(); j++){
                //Same number
                score += game.getListOccurrence().get(i).get(j);
                if(cell_empty(i)) //not validated yet
                    game.getTable().setValueAt(score, i, player_index+1);
            }
        }
        if(tripleDice && doubleDice){ //not validated yet
            game.getTable().setValueAt(25,8,player_index+1);
        }

    }
    /*Calculation three of a kind and four of a kind*/
    private static void calc_bonus_kind(){
        boolean three_of_kind = false;
        boolean four_of_kind = false;
        int score = 0;
        for(int i = 0; i < game.getListDices().getDices().length; i++)
            score += game.getListDices().getDicesIdx(i);
        for(int i = 0; i < game.getListOccurrence().size(); i++){
            for(int j = 0; j < game.getListOccurrence().get(i).size(); j++){
                //Kind of king
                if(game.getListOccurrence().get(i).size() >= 3 && cell_empty(6)) three_of_kind = true;
                if(game.getListOccurrence().get(i).size() >= 4 && cell_empty(6)) four_of_kind = true;
            }
        }
        if(three_of_kind){
            game.getTable().setValueAt(score, 6,player_index+1);
        }
        if(four_of_kind) {
            game.getTable().setValueAt(score, 7,player_index+1);
        }
    }
    /*Calculation large and small straights*/
    private static void calc_straight(){
        if(cell_empty(9)) calc_small_straight();
        if(cell_empty(10)) calc_large_straight();
    }
    /*Calculation large straight*/
    private static void calc_large_straight(){
        ArrayList<Integer> listOfDicesValue = new ArrayList<>();
        for(int i = 0; i < game.getListDices().getDices().length; i++){
            listOfDicesValue.add(game.getListDices().getDicesIdx(i));
        }
        Collections.sort(listOfDicesValue); //sort list asc
        // 1 2 3 4 5
        if (listOfDicesValue.get(0) == 1 &&
                listOfDicesValue.get(1)  == 2 &&
                listOfDicesValue.get(2)  == 3 &&
                listOfDicesValue.get(3)  == 4 &&
                listOfDicesValue.get(4)  == 5) {
            game.getTable().setValueAt(40, 10, player_index+1);
        }
        // 2 3 4 5 6
        else if (listOfDicesValue.get(0) == 2 &&
                listOfDicesValue.get(1)  == 3 &&
                listOfDicesValue.get(2)  == 4 &&
                listOfDicesValue.get(3)  == 5 &&
                listOfDicesValue.get(4)  == 6) {
            game.getTable().setValueAt(40, 10, player_index+1);
        }
    }
    /*Calculation small straight*/
    private static void calc_small_straight(){
        ArrayList<Integer> listOfDicesValue = new ArrayList<>();
        int step = 0;

        for(int i = 0; i < game.getListDices().getDices().length; i++){
            step = 0;
            for(int j = 0; j < listOfDicesValue.size(); j++){
                if(game.getListDices().getDicesIdx(i) == listOfDicesValue.get(j)) step++;
            }
            if(step < 1)
                listOfDicesValue.add(game.getListDices().getDicesIdx(i));
        }
        Collections.sort(listOfDicesValue);
        // 1 2 3 4
        if(listOfDicesValue.size() > 3) {
            if (listOfDicesValue.get(0) == 1 &&
                    listOfDicesValue.get(1) == 2 &&
                    listOfDicesValue.get(2) == 3 &&
                    listOfDicesValue.get(3) == 4) {
                game.getTable().setValueAt(30, 9, player_index + 1);
            }
            // 2 3 4 5
            else if (listOfDicesValue.get(0) == 2 &&
                    listOfDicesValue.get(1) == 3 &&
                    listOfDicesValue.get(2) == 4 &&
                    listOfDicesValue.get(3) == 5) {
                game.getTable().setValueAt(30, 9, player_index + 1);
            }
            // 3 4 5 6
            else if (listOfDicesValue.get(0) == 3 &&
                    listOfDicesValue.get(1) == 4 &&
                    listOfDicesValue.get(2) == 5 &&
                    listOfDicesValue.get(3) == 6) {
                game.getTable().setValueAt(30, 9, player_index + 1);
            }
        }
    }

    /**
     * PAINT DICES FUNCTIONS
     */
    /*Print game board*/
    private static void paint_board(){
        //Paint dices on board
        init_paint_dices();
        //Add line
        paint_line();
        //Add first button roll
        add_button(spaceBoard + windowGame.getWidth() / 8, windowGame.getContentPane().getHeight() - spaceBoard * 2, 190, 50, game.getRollButton(0));
        //Add first player's name
        paint_name(game.getPlayer(0).getName(), windowGame.getContentPane().getHeight() - spaceBoard * 2);
        if(multiplayer) {
            //Add button
            add_button(spaceBoard + windowGame.getWidth() / 8, spaceBoard * 2, 190, 50, game.getRollButton(1));
            //Add User name
            paint_name(game.getPlayer(0).getName(),  spaceBoard * 2);
        }
        //Add score table
        paint_score_board();
        //Add return button
        paint_return_button();

    }
    /*Paint the score board*/
    private static void paint_score_board(){

        JScrollPane scrollPane = new JScrollPane(game.getTable());
        game.getTable().setRowHeight(30);
        for (int i = 0; i < 3; i++) {
            TableColumn column = game.getTable().getColumnModel().getColumn(i);
            if (i == 1 || i == 2) {
                column.setPreferredWidth(10); //sport column is bigger
            } else {
                column.setPreferredWidth(50);
            }
        }

        game.getTable().setBorder(BorderFactory.createLineBorder(Color.black));

        game.getTable().setFont(game.getTable().getFont ().deriveFont (Font.BOLD));

        scrollPane.setBounds(windowGame.getWidth()-windowGame.getWidth()/2+spaceBoard,
                windowGame.getHeight()/2-windowGame.getHeight()/3,
                game.getTable().getColumnModel().getTotalColumnWidth(),
                game.getTable().getRowCount()*30+30);

        if(!multiplayer) game.getTable().removeColumn(game.getTable().getColumnModel().getColumn(2));

        windowGame.getContentPane().add(scrollPane);
    }
    /*Paint line*/
    private static void paint_line(){
        //Add line
        Rectangle line = new Rectangle(spaceBoard, windowGame.getHeight() - spaceBoard*4,
                windowGame.getWidth()/2-spaceBoard,
                3);

        JPanel linePanel = new JPanel();
        linePanel.setBounds(line);
        linePanel.setBackground(Color.black);
        windowGame.add(linePanel);
        if(multiplayer) {
            Rectangle line2 = new Rectangle(spaceBoard, spaceBoard * 4,
                    windowGame.getWidth() / 2 - spaceBoard,
                    3);
            JPanel linePanel2 = new JPanel();
            linePanel2.setBounds(line2);
            linePanel2.setBackground(Color.black);
            windowGame.add(linePanel2);
        }


    }
    /*Initialisation paint all dices*/
    private static void init_paint_dices(){
        enable_dice(false); //locked dice
        for(int i = 0; i < game.getListDices().getButton().length; i++){
            if(game.getTurn() == 0){ //dice locked or first roll
                game.getListDices().setLockedIdx(i, false);//unlocked dice[i]
                windowGame.getContentPane().add(game.getListDices().getButtonIdx(i)); //add button to frame
                game.getListDices().getButtonIdx(i).setIcon(new ImageIcon("assets\\"+game.getListDices().getDicesIdx(i)+".png")); //load image
                //set init position of dices
                paint_locked_dice(i);
            }
        }
        windowGame.repaint();
    }
    /*Paint locked dice with his index*/
    private static void paint_locked_dice(int i){
        //game.getListDices().getButtonIdx(i).setIcon(new ImageIcon("assets\\"+game.getListDices().getDicesIdx(i)+".png"));
        if(player_index == 0)
            game.getListDices().getButtonIdx(i).setBounds(windowGame.getWidth() / 14 + spaceBoard + i * spaceBoard, windowGame.getContentPane().getHeight() - spaceBoard*3 , 50,50);
        else
            game.getListDices().getButtonIdx(i).setBounds(windowGame.getWidth() / 14 + spaceBoard + i * spaceBoard, spaceBoard*3 , 50,50);
    }
    /*Find the new position of dice with his index*/
    private static void new_position_dice_image(int i){
            int newX, newY;
            boolean stop = true;
            int found = 0;
            //find new coordinates
            do {
                found = 0;
                //new coordinates
                newX = ThreadLocalRandom.current().nextInt(spaceBoard, windowGame.getWidth()/2-spaceBoard + 1);
                newY = ThreadLocalRandom.current().nextInt(windowGame.getHeight()/3, windowGame.getHeight()/2 + 1);
                Rectangle rectangleCollision = new Rectangle(newX, newY, 50, 50); //Rect to see collision
                for (int j = 0; j < game.getListDices().getButton().length; j++) {
                    //if no collision
                    if (!rectangleCollision.intersects(new Rectangle(game.getListDices().getButtonIdx(j).getBounds()))) {
                        found++; //need found == i+1 to stop while loop
                        if (found == game.getListDices().getButton().length) stop = false; //new coordinates are good
                    }
                }
            } while (stop);
            //found !
            game.getListDices().getButtonIdx(i).setIcon(new ImageIcon("assets\\"+game.getListDices().getDicesIdx(i)+".png"));
            game.getListDices().getButtonIdx(i).setBounds(newX, newY, 50, 50);
            windowGame.repaint();
    }
    /*Update paint dice*/
    private static void update_paint_dice(){
        for(int i = 0; i < game.getListDices().getButton().length; i++){
            if(game.getListDices().getLockedIdx(i)){ //dice locked
                paint_locked_dice(i);
            }
            else new_position_dice_image(i); //dice unlocked
        }
    }
    /*Paint return button*/
    private static void paint_return_button(){
        JButton returnButton = new JButton("Return");
        add_button(windowGame.getWidth()-spaceBoard*2, spaceBoard-spaceBoard/2, 100,20,returnButton);
    }
    /*Paint player's names*/
    private static void paint_name(String name, int y){
        JLabel userName = new JLabel(game.getPlayer(0).getName());
        userName.setFont (userName.getFont ().deriveFont (16.0f));
        userName.setBounds(spaceBoard+windowGame.getWidth()/8, y, 190, 50);
        windowGame.add(userName);
    }

    /**
     * EXTERNAL FUNCTIONS
     */
    /*Get user name*/
    private static String input_user(){
        //input name user
        String name = new String();
        name =  JOptionPane.showInputDialog(
                windowGame,
                "What is your name ? ",
                "Player name",
                JOptionPane.OK_CANCEL_OPTION);
        System.out.println(name);
        if(name == null){
            clean_frame(); //remove frame
            init_frame_game(); //load menu*/
            return null;
        }
        return name;
    }
    /*Remove all components*/
    private static void clean_frame(){
        //clean frame
        windowGame.getContentPane().removeAll();
        windowGame.getContentPane().setBackground(boardColor);
        windowGame.getContentPane().setLayout(null);
        windowGame.repaint();
    }
    /*Created a button with coordinate and size => add to content panel*/
    private static void add_button(int x, int y, int w, int h, JButton button){
        button.setBounds(x,y,w,h);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        //game.rollButton.setContentAreaFilled(false);
        windowGame.getContentPane().add(button);
        button.addActionListener(new Action());
    }
    /*Enable or disable dices*/
    private static void enable_dice(boolean value){
        for(int i = 0; i < game.getListDices().getDices().length; i++){
            game.getListDices().getButtonIdx(i).setEnabled(value);
        }
    }
    /*return true or false if the cell is empty or not*/
    private static boolean cell_empty(int row){
        String value = game.getData(row, player_index+3).toString();
        if(value.equals("false")) return true;
        return false;
    }

    /**
     *  EXTERNAL CLASS
     */
    /*Class action command with button*/
    private static class Action implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String name = e.getActionCommand();
            //Switch menu
            switch(name){
                case "Play":
                    multiplayer_panel();
                    break;
                case "Help":
                    helper();
                    break;
                case "Quit":
                    System.exit(0);
                    break;
                case "Roll Dice":
                    game.getListDices().reRoll(); //re roll dice
                    game.setTurn(); //add 1 turn
                    update_paint_dice(); //update frame and repaint dice
                    update_game(); //update game
                    break;
                case "Return":
                    clean_frame(); //remove frame
                    init_frame_game(); //load menu
                    multiplayer = false;
                    break;
                case "Multiplayer":
                    multiplayer = true;
                    run_game();
                    break;
                case "Soloplayer":
                    multiplayer = false;
                    run_game();
                    break;
            }
            if(name.contains("Dice=")){ //if click on dice
                String clean = name.replaceAll("\\D+",""); //remove non-digits
                game.getListDices().setLockedIdx(Integer.parseInt(clean), !game.getListDices().getLockedIdx(Integer.parseInt(clean))); //change locked value
                if(game.getListDices().getLockedIdx(Integer.parseInt(clean))) paint_locked_dice(Integer.parseInt(clean)); //if locked
                else new_position_dice_image(Integer.parseInt(clean)); //else random position
            }
        }
    }

    /**
     * Helper FUNCTIONS
     */
    private static void helper(){
        clean_frame();
        paint_return_button();
        JTextArea textGoalArea = new JTextArea(
                "\t     **GOAL**\n\n\nThe objective of the game is to score points\n" +
                        " by rolling five dice to make certain combinations. \n" +
                        "The dice can be rolled up to three times\n" +
                        " in a turn to try to make various scoring combinations\n" +
                        " and dice must remain in the box. \n" +
                        "A game consists of thirteen rounds. \n" +
                        "After each round the player chooses which scoring category is to be used\n" +
                        " for that round. \n" +
                        "Once a category has been used in the game, it cannot be used again. \n" +
                        "The scoring categories have varying point values, \n" +
                        "some of which are fixed values and others for which \n" +
                        "the score depends on the value of the dice. \n" +
                        "The winner is the player who scores the most points. ");

        textGoalArea.setLineWrap(true);
        textGoalArea.setWrapStyleWord(true);
        textGoalArea.setBounds(70,150,400,400);
        Font textFont = new Font("Serif", Font.BOLD, 15);
        textGoalArea.setFont(textFont);
        windowGame.add(textGoalArea);
        windowGame.setVisible(true);
        windowGame.setLayout(null);
        windowGame.repaint();


        JTextArea textFigureArea = new JTextArea(
                "\t**Different Figures**\n\n\nAces : The sum of dice with the number 1\n" +
                        "Twos : The sum of dice with the number 2\n" +
                        "Threes : The sum of dice with the number 3\n" +
                        "Fours : The sum of dice with the number 4 \n" +
                        "Fives : The sum of dice with the number 5\n" +
                        "Sixes : The sum of dice with the number 6\n" +
                        "\nThree of a king : At least three dice the same (sum)\n" +
                        "Four of a king : At least four dice the same (sum)\n"+
                        "Full House : Three of one number and two of another\n"+
                        "Small Straight: Four sequential dice (1-2-3-4, 2-3-4-5, or 3-4-5-6)\n" +
                        "Large Straight: Five sequential dice (1-2-3-4-5 or 2-3-4-5-6)\n" +
                        "Yahtzee : All five dice the same\n" +
                        "Chance : Any Combination");

        textFigureArea.setLineWrap(true);
        textFigureArea.setWrapStyleWord(true);
        textFigureArea.setBounds(500,150,400,400);
        textFigureArea.setFont(textFont);
        windowGame.add(textFigureArea);
        windowGame.setVisible(true);
        windowGame.setLayout(null);
        windowGame.repaint();

        JLabel title = new JLabel();
        // add text to label
        title.setText("Helper");
        title.setFont (title.getFont ().deriveFont (64.0f));
        title.setBounds(windowGame.getWidth()/2-windowGame.getWidth()/8, spaceBoard, windowGame.getWidth()/4,spaceBoard);

        // add label to panel
        windowGame.add(title);



    }



}

